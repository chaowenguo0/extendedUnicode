public class ExtendedUnicode
{
    public static void main(final java.lang.String[] args)
    {
        java.lang.System.out.printf("%s %s %s", "𠮷", java.lang.Character.toString(0x20BB7), java.lang.Integer.toString("𠮷".codePointAt(0), 16));
        java.lang.System.out.println();
    }
}
