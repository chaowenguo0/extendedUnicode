#include <iostream>

int main()
{
    std::cout << "\U00020BB7" << "𠮷" << std::u32string(1, static_cast<char32_t>(0x20BB7)) << std::hex << static_cast<int>(U'𠮷') << std::endl;
}
